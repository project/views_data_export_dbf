Views Data Export DBF
=====================

Introduction
------------

This module is designed to provide a style plugin for Views Data Export module
that support exporting DBF files.

Using the "Views Data Export" module
------------------------------------

1. Add a new "Data export" display to your view.
2. Change its "Style" to the DBF export type.
3. Configure the options (such as name, quote, etc.). You can go back and do
   this at any time by clicking the gear icon next to the style plugin you just
   selected.
4. Give it a path in the Feed settings such as "path/to/view/dbf".
5. Optionally, you can choose to attach this to another of your displays by
   updating the "Attach to:" option in feed settings.

For more information see Views Data Export module README.txt.
