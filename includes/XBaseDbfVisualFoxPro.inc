<?php
/**
 * @file
 * Contains XBaseDbfVisualFoxPro class that handles Visual FoxPro DBF files.
 */

/**
 * Handler for VisualFoxPro
 *
 * @todo Add support to CDX files.
 */
class XBaseDbfVisualFoxPro extends XBaseDbfDBaseIII {
  public $languageCode;
  public $backlink;

  /**
   * Creates a new Visual FoxPro DBF database.
   *
   * @param string $file
   *   The path of the file (stream or normal path).
   *
   * @param array $fields
   *   Accepted values (in order): name, type, length and precision.
   *
   * @see XBaseDbfDBaseIII::create()
   */
  static function create($file, $fields, $options) {
    $table = new XBaseDbfVisualFoxPro($file, "w+");
    $table->setHeader($options);
    // We don't have any columns yet so we set them to the object before writing header.
    $table->setHeaderFields($fields);

    // We set the header length only at file creation because columns cannot
    // change after file is created. Setting it here allows other subclasses
    // to to change it if needed and still use XBaseDbfDBaseIII::writeHeader().
    //
    // fixed header bytes + backlink bytes + column number * field descriptors number
    $table->headerLength = 32 + $table->getColumnCount() * 32 + 1 + 263;
    $table->writeHeader();

    return $table;
  }

  static function open($file) {
    $table = new XBaseDbfVisualFoxPro($file, "rb");
    $table->readHeader();
    // Field description array is equal with header length minus fixed 33 bytes
    // divided by 32 (number of description bytes for each field).
    $fieldCount = ($table->headerLength - 296) / 32;
    $table->readHeaderFields($fieldCount);
    $offset = $table->headerLength - 263;
    $table->backlink = $table->readBytes(263, $offset);

    return $table;
  }

  static function openWrite($file) {
    // Open for reading and writing; place the file pointer at the beginning
    // of the file.
    $table = new XBaseDbfVisualFoxPro($file, "r+");
    $table->readHeader();

    // @see XBaseDbfDBaseIII::open()
    $fieldCount = ($table->headerLength - 263 - 1 - 32) / 32;
    $table->readHeaderFields($fieldCount);

    return $table;
  }

  static function getLanguageCodes() {
    return array(
        '01' => 'U.S. MS-DOS',
        '69' => 'Polish (Mazovia) MS-DOS',
        '6A' => 'Greek MS-DOS (437G)',
        '02' => 'International MS-DOS',
        '64' => 'Eastern European MS-DOS',
        '6B' => 'Turkish MS-DOS',
        '67' => 'Icelandic MS-DOS',
        '66' => 'Nordic MS-DOS',
        '65' => 'Russian MS-DOS',
        '7C' => 'Thai Windows',
        '68' => 'Czech (Kamenicky) MS-DOS',
        '7B' => 'Japanese Windows',
        '7A' => 'Chinese (PRC, Singapore) Windows',
        '79' => 'Korean Windows',
        '78' => 'Chinese (Hong Kong SAR, Taiwan) Windows',
        'C8' => 'Eastern European Windows',
        'C9' => 'Russian Windows',
        '03' => 'Windows ANSI',
        'CB' => 'Greek Windows',
        'CA' => 'Turkish Windows',
        '7D' => 'Hebrew Windows',
        '7E' => 'Arabic Windows',
        '04' => 'Standard Macintosh',
        '98' => 'Greek Macintosh',
        '96' => 'Russian Macintosh',
        '97' => 'Macintosh EE',
    );
  }
  function __construct($file, $mode) {
    parent::openFile($file, $mode);
  }

  function setHeader($options) {
    parent::setHeader($options);

    $lang_codes = array_keys($this->getLanguageCodes());
    $language_exists = in_array($options['languageCode'], $lang_codes);
    $this->languageCode = $language_exists ? $options['languageCode'] : chr(0);
  }

  function writeHeader() {
    parent::writeHeader();
    // Language code
    $this->writeHex($this->languageCode, 29);

    //@todo [COLUMNS: Field data address] In FoxPro Offset of field from
    //      beginning of record.

    // No CDX file.
    $this->writeHex('00', 28);

    // Backlink
    $offset = $this->headerLength - 263;
    $this->writeBytes(str_pad($this->backlink, 263, chr(0)), $offset);
  }

  /**
   * Writes field offset in memAddress for each columns (required only for Fox).
   *
   * @param array $fields
   */
  protected function setHeaderFields($fields) {
    // First set all columns
    parent::setHeaderFields($fields);

    // We don't use $this->getColumns() because columns are not passed by
    // reference.
    foreach ($this->columns as $id => &$column) {
      // Offset in FoxPro is in fact the XBaseDbfColumn::bytePos.
      $column->memAddress = $column->getBytePos();
    }
  }
}
