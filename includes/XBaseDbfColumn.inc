<?php
/**
 * @file
 * Contains the XBaseDbfColumn object definition.
 */

/**
 * Column object definition.
 *
 * @todo More comments on the code.
 */
class XBaseDbfColumn {

  public $name;
  public $rawname;
  public $type;
  public $memAddress;
  public $length;
  public $decimalCount;
  public $workAreaID;
  public $setFields;
  public $indexed;
  public $bytePos;
  public $colIndex;

  /**
   * Constructor for DBF column object.
   */
  function __construct(
      $name,
      $type,
      $memAddress,
      $length,
      $decimalCount,
      $reserved1,
      $workAreaID,
      $reserved2,
      $setFields,
      $reserved3,
      $indexed,
      $colIndex,
      $bytePos
  ) {
    $this->rawname = $name;
    $this->name = strpos($name, 0x00) !== FALSE ? substr($name, 0, strpos($name, 0x00)) : $name;
    $this->type = $type;
    $this->memAddress = $memAddress;
    $this->length = $length;
    $this->decimalCount = $decimalCount;
    $this->workAreaID = $workAreaID;
    $this->setFields = $setFields;
    $this->indexed = $indexed;
    $this->bytePos = $bytePos;
    $this->colIndex = $colIndex;
  }

  /**
   * Gets number of decimals for number columns.
   */
  function getDecimalCount() {
    return $this->decimalCount;
  }

  /**
   * Checks if column is inexed.
   */
  function isIndexed() {
    return $this->indexed;
  }

  /**
   * Gets length of the column.
   */
  function getLength() {
    return $this->length;
  }

  /**
   * Gets data (bytes) length for o column for columns known types.
   *
   * @return int
   *   The known length of data or readed length.
   */
  function getDataLength() {
    switch ($this->type) {
      case DBFFIELD_TYPE_DATE:
      case DBFFIELD_TYPE_DATETIME:
        return 8;

      case DBFFIELD_TYPE_LOGICAL:
        return 1;

      case DBFFIELD_TYPE_MEMO:
        return 10;

      default:
        return $this->length;
    }
  }

  /**
   * Gets memory address of the column or field offset for Visual FoxPro.
   */
  function getMemAddress() {
    return $this->memAddress;
  }

  /**
   * Gets name of the column.
   */
  function getName() {
    return $this->name;
  }

  /**
   * Checks if fields are set.
   */
  function isSetFields() {
    return $this->setFields;
  }

  /**
   * Gets the type of th column.
   */
  function getType() {
    return $this->type;
  }

  /**
   * Gets the work areea id of the column.
   */
  function getWorkAreaID() {
    return $this->workAreaID;
  }

  /**
   * Converts the name of the column to string.
   *
   * @todo Ensure that we return a string.
   */
  function toString() {
    return $this->name;
  }

  /**
   * Gets the byte position of the column.
   */
  function getBytePos() {
    return $this->bytePos;
  }

  /**
   * Gets the column row name.
   */
  function getRawname() {
    return $this->rawname;
  }

  /**
   * Gets the column index.
   */
  function getColIndex() {
    return $this->colIndex;
  }
}
