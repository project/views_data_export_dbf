<?php
/**
 * @file
 * Contains XBaseDbfDBaseIII class that handles Dbase III DBF files.
 */

/**
 * Handler for dBaseIII+.
 *
 * @todo Add support to memo files.
 */
class XBaseDbfDBaseIII extends XBaseDbfCore {
  /**
   * Creates a new dBase III DBF database.
   *
   * @see XBaseDbf::create()
   */
  static function create($file, $fields, $options) {
    // Open for reading and writing; place the file pointer at the beginning of
    // the file and truncate the file to zero length. If the file does not
    // exist, attempt to create it.
    $table = new XBaseDbfDBaseIII($file, "r+");

    // We don't have any columns yet so we set them to the object before
    // writing header.
    $table->setHeaderFields($fields, $options);

    // We set the header length only at file creation because columns cannot
    // change after file is created. Setting it here allows other subclasses
    // to to change it if needed and still use XBaseDbfDBaseIII::writeHeader().
    //
    // fixed header bytes + backlink bytes + column number * field descriptors number
    $table->headerLength = 33 + $table->getColumnCount() * 32;
    $table->writeHeader();

    return $table;
  }

  /**
   * Open a dBase III DBF in read only mode.
   *
   * @see XBaseDbf::open()
   */
  static function open($file) {

    $table = new XBaseDbfDBaseIII($file, "rb");
    $table->readHeader();

    // Field description array is equal with header length minus fixed 33 bytes
    // divided by 32 (number of description bytes for each field.
    $fieldCount = ($table->headerLength - 33) / 32;
    $table->readHeaderFields($fieldCount);

    return $table;
  }

  /**
   * Open a dBase III DBF in write mode.
   *
   * @see XBaseDbf::openWrite()
   */
  static function openWrite($file) {
    // Open for reading and writing; place the file pointer at the beginning
    // of the file.
    $table = new XBaseDbfDBaseIII($file, "r+");
    $table->readHeader();

    // @see XBaseDbfDBaseIII::open()
    $fieldCount = ($table->headerLength - 33) / 32;
    $table->readHeaderFields($fieldCount);

    return $table;
  }

  /**
   * Constructor for DBase III object.
   *
   * @param string $file
   *   The file path or stream.
   *
   * @param string $mode
   *   The mode file needs to be opened.
   */
  function __construct($file, $mode) {
    parent::openFile($file, $mode);
  }

  /**
   * Set header of DBF object.
   *
   * @param array $options
   *   Associative array of DBF options.
   *
   * @see XBaseDbf::create()
   */
  protected function setHeader($options = array()) {
    $this->version = isset($options['version']) ? $options['version'] : 83;
    $this->modifyDate = isset($options['modifyDate']) ? $options['modifyDate'] : time();
    $this->recordCount = 0;
    // $this->inTransaction = 0;
    // $this->encrypted = FALSE;
  }

  /**
   * Reads header of DBF file.
   */
  protected function readHeader() {
    $this->version = $this->readHex(0);
    $this->modifyDate = $this->read3ByteDate(1);
    $this->recordCount = $this->readInt(4);
    $this->headerLength = $this->readShort(8);
    $this->recordByteLength = $this->readShort(10);

    // Only in dBaseIV.
    // $this->inTransaction = $this->readBytes(1, 14) != 0;
    // $this->encrypted = $this->readBytes(1, 15) != 0;
    // $this->mdxFlag = $this->readBytes(1, 28);

    // Only in fox.
    // $this->languageCode = $this->readHex(29);
  }

  /**
   * Writes header to DBF file.
   */
  protected function writeHeader() {
    $this->writeHex($this->version, 0);
    $this->write3ByteDate($this->modifyDate);
    $this->writeInt($this->recordCount);
    $this->writeShort($this->headerLength);
    $this->writeShort($this->recordByteLength);
    // Reserved.
    $this->writeBytes(str_pad("", 2, chr(0)));
    // Reserved transaction in dBaseIV.
    $this->writeBytes(str_pad("", 1, chr(0)));
    // Reserved encryption in dBaseIV.
    $this->writeBytes(str_pad("", 1, chr(0)));
    // Free record thread (reserved for LAN only).
    $this->writeBytes(str_pad("", 4, chr(0)));
    // Reserved for LAN only multi-user dBASE (dBASE III+).
    // @todo What is this?
    $this->writeBytes(str_pad("", 8, chr(0)));
    // Reserved MDX flag (dBASE IV).
    $this->writeBytes(str_pad("", 1, chr(0)));
    // Reserved language driver in Visual FoxPro.
    $this->writeBytes(str_pad("", 1, chr(0)));
    // Reserved.
    $this->writeBytes(str_pad("", 2, chr(0)));
    foreach ($this->columns as $column) {
      // Field name in ASCII terminated by 00h.
      $this->writeString(str_pad(substr($column->rawname, 0, 11), 11, chr(0)));
      // Field type (ASCII).
      $this->writeBytes($column->type);
      // Field data address in memory.
      // @todo In FoxPro Offset of field from beginning of record.
      $this->writeInt($column->memAddress);
      // Field length.
      $this->writeChar($column->getDataLength());
      // Decimal count.
      $this->writeChar($column->decimalCount);
      // Reserved for multi-user dBASE.
      $this->writeBytes(str_pad("", 2, chr(0)));
      // Work area ID.
      $this->writeChar($column->workAreaID);
      // Reserved for multi-user dBASE.
      $this->writeBytes(str_pad("", 2, chr(0)));
      // Flag for SET FIELDS.
      $this->writeBytes(chr($column->setFields ? 1 : 0));
      // Reserved.
      $this->writeBytes(str_pad("", 7, chr(0)));
      // Index field flag.
      $this->writeBytes(chr($column->indexed ? 1 : 0));
    }
    $this->writeHex('0D');
  }

  /**
   * Sets the header fields (columns).
   *
   * @param array $fields
   *   Array of header columns.
   *
   * @see XBaseDbf::create()
   */
  protected function setHeaderFields($fields) {
    $recordByteLength = 1;
    $columns = array();
    $columnNames = array();
    $i=0;
    foreach ($fields as $field) {
      if (!$field || !is_array($field) || sizeof($field) < 2) {
        trigger_error("Fields argument error, must be array of arrays.", E_USER_ERROR);
      }
      // Reindex field because we care about values not keys name.
      $field = array_values($field);
      // @todo Which is the correct memAddress for DBase III?
      $column = new XBaseDbfColumn($field[0], $field[1], 0, @$field[2], @$field[3], 0, 0, 0, 0, 0, 0, $i, $recordByteLength);
      $recordByteLength += $column->getDataLength();
      $this->columnNames[$i] = $field[0];
      $this->columns[$i] = $column;
      $i++;
    }
    $this->recordByteLength = $recordByteLength;
  }

  /**
   * Reads the headers fields and sets them to the object.
   *
   * @param int $fieldCount
   *   Number of columns.
   *
   * @todo - Not sure that number of columns is needed here.
   *       - Maybe we should move this method to XBaseDbfCore.
   */
  protected function readHeaderFields($fieldCount) {
  // Make sure that we reset columns names and parameters every time we read
  // header.
  $this->columnNames = array();
  $this->columns = array();
  $bytepos = 1;
  for ($i = 0; $i < $fieldCount; $i++) {
  // 32 start offset + 32 bytes of field descriptors * current field
    $offset = 32 + 32 * $i;
    // @todo we can set column and than set is props.
    $column = new XBaseDbfColumn(
        //  [0 - 10] Field name in ASCII (terminated by 00h).
        $this->readBytes(11, $offset),
        // [11] Field type (ASCII).
        $this->readBytes(),
        // [12 - 15] Field data address.
        $this->readInt(),
        // [16] Field length
        $this->readChar(),
        // [17] Decimal count.
        $this->readChar(),
        // [18 - 19] Reserved for multi-user dBASE.
        $this->readBytes(2),
        // [20] Work area ID.
        $this->readChar(),
        // [21 - 22] Reserved for multi-user dBASE.
        $this->readBytes(2),
        // [23] Flag for SET FIELDS
        $this->readBytes() != 0,
        // [24 - 30] Reserved.
        $this->readBytes(7),
        // [31] Index field flag.
        $this->readBytes() != 0,
        // Column index.
        $i,
        // Byte position.
        $bytepos
    );
    $bytepos += $column->getLength();
    $this->columnNames[$i] = $column->getName();
    $this->columns[$i] = $column;
  }
  }
}
