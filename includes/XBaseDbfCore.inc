<?php
/**
 * @file
 * Contains XBaseDbfCore class required by all DBF handlers.
 */

/**
 * Define our required constants.
 */
// Memo type field.
define("DBFFIELD_TYPE_MEMO", "M");

// Character type field.
define("DBFFIELD_TYPE_CHAR", "C");

// Numeric type field.
define("DBFFIELD_TYPE_NUMERIC", "N");

// Floating point field.
define("DBFFIELD_TYPE_FLOATING", "F");

// Date type field.
define("DBFFIELD_TYPE_DATE", "D");

// Logical type field - ? Y y N n T t F f (? when not initialized).
define("DBFFIELD_TYPE_LOGICAL", "L");

// DateTime type field.
define("DBFFIELD_TYPE_DATETIME", "T");

// Index type field.
define("DBFFIELD_TYPE_INDEX", "I");

// Ignore this field.
define("DBFFIELD_IGNORE_0", "0");

/**
 * Core class for DBF files.
 *
 * All DBF database classes are extended from this class.
 *
 * @todo Finish the comments on code.
 */
class XBaseDbfCore {
  /**
   * DBF file path.
   *
   * @var string
   */
  public $name;

  /**
   * Private file pointer. No child class should be able to change this
   * directly.
   *
   * @var file resource
   */
  private $fp;

  /**
   * DBF version.
   *
   * @var hex
   */
  public $version;

  /**
   * DBF last changed time in unix format.
   *
   * @var int
   */
  public $modifyDate;

  /**
   * Number of records in DBF file.
   *
   * @var int
   */
  public $recordCount;

  /**
   * Columns objects in DBF file.
   *
   * @var array
   */
  public $columns = array();

  /**
   * Columns names in DBF file.
   *
   * @var array
   */
  public $columnNames = array();

  /**
   * Length of header.
   *
   * @var int
   */
  public $headerLength;

  /**
   * Length of each record.
   *
   * @var int
   */
  public $recordByteLength;

  /**
   * In transaction flag.
   *
   * @var bool
   *
   * @todo How to handle if the file is in transaction or not.
   */
  private $inTransaction;

  /**
   * Encryption flag.
   *
   * @var bool
   *
   * @todo How to handle if the file is encrypted or not.
   */
  private $encrypted;

  /**
   * MDX flag.
   *
   * @var bool
   *
   * @todo Maybe we should move this in DbaseIV handler when it will be ready.
   */
  private $mdxFlag;

  /**
   * Gets records from file.
   *
   * @param int $count
   *   The number of records to get or all records if not specified or is 0.
   *
   * @param int $offset
   *   Return the records from offset start point or from start of file if not
   *   specified or is 0.
   *
   * @return array
   *   Array of found records objects.
   *
   * @see XBaseDbfCore::getRecord()
   */
  function getRecords($count = 0, $offset = 0) {
    if ($offset >= $this->recordCount) {
      return FALSE;
    }
    if ($count == 0) {
      $count = $this->recordCount;
    }
    $records = array();
    for ($i = $offset; $i < $this->recordCount && $i < $offset + $count; $i++) {
      $record = array();
      $record_object = $this->getRecord($i);
      foreach ($this->getColumns() as $index => $column) {
        $record[$index] = $record_object->getString($column);
      }
      $records[$i] = $record;
    }
    return $records;
  }

  /**
   * Get the record from offset.
   *
   * @param int $record_offset
   *   Position of record to return or the first record if not specified.
   *
   * @return object
   *   The record object.
   */
  function &getRecord($record_offset = 0) {
    /*
    if (!$this->isOpen()) {
      $this->open();
    }
    */

    $byte_offset = $this->headerLength + $record_offset * $this->recordByteLength;
    $record = new XBaseDbfRecord($this, $record_offset, $this->readBytes($this->recordByteLength, $byte_offset));

    /*
    if ($this->record->isDeleted()) {
      $this->deleteCount++;
    }
    */

    return $record;
  }

  /**
   * Append records to file.
   *
   * @param array $records
   *   Array of records.
   *
   * @see XBaseDbfCore::appendRecord()
   */
  function appendRecords($records) {
    foreach ($records as $record) {
      $this->appendRecord($record);
    }
  }

  /**
   * Append a record to file.
   *
   * @param array $record
   *   An associative array with key the index of the column and value the
   *   the content of that column.
   */
  function appendRecord($record) {
    $record_object = new XBaseDbfRecord($this, $this->recordCount);
    if (count($record) != $this->getColumnCount()) {
      trigger_error('Wrong record recieved.');
    }
    // Reindex field because we care about values not keys name.
    $record = array_values($record);
    foreach ($record as $id => $cell) {
      $record_object->setObjectByIndex($id, $cell);
    }
    fseek($this->fp, $this->headerLength + ($this->recordCount * $this->recordByteLength));
    $data = &$record_object->serializeRawData();
    fwrite($this->fp, $data);
    $this->recordCount++;
    if ($record_object->inserted) {
      // Make sure we put 0x1A at the end of file.
      $this->writeHex('1A');
      $this->writeHeader();
    }
    // Force immediate write to file.
    fflush($this->fp);
  }

  /**
   * Gets the column names.
   *
   * @return array
   *   Aassociative array of column names with column index as key.
   */
  function getColumnNames() {
    return $this->columnNames;
  }

  /**
   * Gets the column objects.
   *
   * @return array
   *   Aassociative array of column objects with column index as key.
   */
  function getColumns() {
    return $this->columns;
  }

  /**
   * Gets the column object by index.
   *
   * @param int $index
   *   The index of the column.
   *
   * @return object
   *   The column object.
   */
  function &getColumn($index) {
    if (isset($this->columns[$index])) {
      return $this->columns[$index];
    }
  }

  /**
   * Gets the column object by name.
   *
   * @param string $name
   *   The name of the column.
   *
   * @return object
   *   The column object.
   */
  function &getColumnByName($name) {
    foreach ($this->columnNames as $i => $n) {
      if (strtoupper($n) == strtoupper($name)) {
        return $this->columns[$i];
      }
    }
    return FALSE;
  }

  /**
   * Gets the column index.
   *
   * @param string $name
   *   The name of the column.
   *
   * @return int
   *   The index of the column.
   */
  function getColumnIndex($name) {
    foreach ($this->columnNames as $i => $n) {
      if (strtoupper($n) == strtoupper($name)) {
        return $i;
      }
    }
    return FALSE;
  }

  /**
   * Gets number of columns.
   *
   * @return int
   *   The number of columns.
   */
  function getColumnCount() {
    return count($this->columns);
  }

  /**
   * Opens the DBF file.
   *
   * @param string $file
   *   File path or stream.
   *
   * @param string $mode
   *   The mode to open file.
   *
   * @return bool
   *   Retruns TRUE on success of FALSE on fail.
   *
   * @see fopen()
   */
  protected function openFile($file, $mode) {
    $this->name = $file;
    $this->fp = fopen($file, $mode);
    // Check if we need to read the header.
    if ($mode != "w+") {
      $this->readHeader();
    }
    return $this->fp != FALSE;
  }

  /**
   * Closes the DBF file.
   */
  public function close() {
    fclose($this->fp);
  }

  /**
   * Read a number of bytes starting from offset in opened file.
   *
   * @param int $length
   *   Length to read.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return string
   *   Readed bytes.
   */
  protected function readBytes($length = 1, $offset = NULL) {
    if (!is_null($offset)) {
      fseek($this->fp, $offset);
    }
    return fread($this->fp, $length);
  }

  /**
   * Writes a number of bytes starting from offset in opened file.
   *
   * @param string $bytes
   *   Bytes to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be write starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Return TRUE on success FALSE on fail.
   *
   * @see fwrite()
   */
  protected function writeBytes($bytes, $offset = NULL) {
    if (!is_null($offset)) {
      fseek($this->fp, $offset);
    }
    return fwrite($this->fp, $bytes);
  }

  /**
   * Read a string starting from offset in opened file.
   *
   * @see XBaseDbfCore::readBytes()
   *
   * @todo Same as readBytes, maybe we should remove it at some point.
   */
  protected function readString($length, $offset = NULL) {
    return $this->readBytes($length, $offset = NULL);
  }

  /**
   * Writes a string starting from offset in opened file in write/append mode.
   *
   * @param string $string
   *   Stromg to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeBytes()
   */
  protected function writeString($string, $offset = NULL) {
    return $this->writeBytes($string, $offset);
  }

  /**
   * Read a single byte char starting from offset in opened file.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return string
   *   Readed char.
   */
  protected function readChar($offset = NULL) {
    $buf = unpack("C", $this->readBytes(1, $offset));
    return $buf[1];
  }

  /**
   * Write a single byte char starting from offset in opened file.
   *
   * @param string $char
   *   Char to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeBytes()
   */
  protected function writeChar($char, $offset = NULL) {
    $buf = pack("C", $char);
    return $this->writeBytes($buf, $offset);
  }

  /**
   * Read a short (2 bytes) starting from offset in opened file.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return string
   *   Readed short.
   *
   *@todo Maybe we need to make sure we return short.
   */
  protected function readShort($offset = NULL) {
    $buf = unpack("S", $this->readBytes(2, $offset));
    return $buf[1];
  }

  /**
   * Write a short (2 bytes) starting from offset in opened file.
   *
   * @param string $short
   *   Short to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeBytes()
   */
  protected function writeShort($short, $offset = NULL) {
    $buf = pack("S", $short);
    return $this->writeBytes($buf, $offset = NULL);
  }

  /**
   * Read an unregistred integer (4 bytes) starting from offset in opened file.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return int
   *   Readed integer.
   *
   * @todo Maybe we need to make sure we return intval not string.
   */
  protected function readInt($offset = NULL) {
    $buf = unpack("I", $this->readBytes(4, $offset));
    return $buf[1];
  }

  /**
   * Write an integer (4 bytes) starting from offset in opened file.
   *
   * @param int $integer
   *   Integer to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeBytes()
   */
  protected function writeInt($integer, $offset = NULL) {
    $buf = pack("I", $integer);
    return $this->writeBytes($buf, $offset);
  }

  /**
   * Read a long (8 bytes) starting from offset in opened file.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return string
   *   Readed long.
   */
  protected function readLong($offset = NULL) {
    $buf = unpack("L", $this->readBytes(8, $offset));
    return $buf[1];
  }

  /**
   * Write an long (8 bytes) starting from offset in opened file.
   *
   * @param string $long
   *   Long to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeBytes()
   */
  protected function writeLong($long, $offset = NULL) {
    $buf = pack("L", $long);
    return $this->writeBytes($buf, $offset);
  }

  /**
   * Read a hex starting from offset in opened file.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return string
   *   Readed hex.
   */
  protected function readHex($offset = NULL) {
    return strtoupper(dechex($this->readChar($offset)));
  }

  /**
   * Write an hex starting from offset in opened file.
   *
   * @param string $hex
   *   Hex to write.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeBytes()
   */
  protected function writeHex($hex, $offset = NULL) {
    return $this->writeBytes(chr(hexdec($hex)), $offset);
  }

  /**
   * Read a 3 bytes date starting from offset in opened file.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return string
   *   Readed date in unix time.
   */
  protected function read3ByteDate($offset = NULL) {
    // We can set offset on first if needed. For the rest file is not rewinded
    // so is not needed.
    $y = unpack("c", $this->readBytes(1, $offset));
    $m = unpack("c", $this->readBytes(1));
    $d = unpack("c", $this->readBytes(1));
    return mktime(0, 0, 0, $m[1], $d[1], $y[1] > 70 ? 1900 + $y[1] : 2000 + $y[1]);
  }

  /**
   * Write a 3 bytes date starting from offset in opened file.
   *
   * @param int $date
   *   Date in unix time format.
   *
   * @param int $offset
   *   Starting from offset. If not specified file will be read starting from
   *   where it left from last read / write operation.
   *
   * @return bool
   *   Readed TRUE on success or FALSE on fail.
   *
   * @see XBaseDbfCore::writeChar()
   */
  protected function write3ByteDate($date, $offset = NULL) {
    $t = getdate($date);
    return $this->writeChar($t["year"] % 1000, $offset) + $this->writeChar($t["mon"]) + $this->writeChar($t["mday"]);
  }

  /**
   * Read a 4 bytes date starting from offset in opened file.
   *
   * @see XBaseDbfCore::read3ByteDate()
   */
  protected function read4ByteDate($offset = NULL) {
    // Short is 2 bytes long.
    $y = $this->readShort($offset);
    $m = unpack("c", $this->readBytes());
    $d = unpack("c", $this->readBytes());
    return mktime(0, 0, 0, $m[1], $d[1], $y);
  }

  /**
   * Writes a 4 bytes date starting from offset in opened file.
   *
   * @see XBaseDbfCore::write3ByteDate()
   */
  protected function write4ByteDate($date, $offset = NULL) {
    $t = getdate($date);
    return $this->writeShort($t["year"], $offset) + $this->writeChar($t["mon"]) + $this->writeChar($t["mday"]);
  }
}
