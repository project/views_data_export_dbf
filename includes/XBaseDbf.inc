<?php
/**
 * @file
 * Contains the XBaseDbf base class.
 *
 * @todo - Not sure what error handler to use. I don't think trigger_error is
 *         the best.
 */

/**
 * Loader class based on file version.
 */
class XBaseDbf {
  /**
   * Creates a new DBF file.
   *
   * @param string $file
   *   The file path or stream.
   *
   * @param array $fields
   *   Array of header columns with values an array with following values:
   *     - name: The name of the column (required).
   *     - type: The type of the colomn (required).
   *     - length: The length of the column (not required for bool and date).
   *     - precision: Required for numeric values.
   *
   * @param array $options
   *   Associative array with DBF options containing:
   *     - version: Version of DBF file in hex.
   *     - modifyDate: Modify date of DBF in unix time format.
   *
   * @return object
   *   The XBaseDbf object from version handler.
   *
   * @todo Make $fields['0'] default to type "C".
   */
  static function create($file, $fields, $options) {
    // Check fields first.
    if (!$fields || !is_array($fields)) {
      trigger_error("Cannot create DBF file with no fields", E_USER_ERROR);
    }

    // If supported file load and return the object else trigger error.
    // @todo extends the check of DBF files.
    $db_types = self::getTypes();
    if (isset($options['version']) &&
        in_array($options['version'], array_keys(self::getTypes()))) {
      $handler = 'XBaseDbf' . $db_types[$options['version']]['handler'];
    }
    else {
      $handler = 'XBaseDbfDBaseIII';
    }
    $object = $handler::create($file, $fields, $options);

    return $object;
  }

  /**
   * Opens a DBF file in readonly mode.
   *
   * @param string $file
   *   The file path or stream.
   *
   * @return object
   *   The XBaseDbf object from version handler.
   */
  static function open($file) {
    $version = self::getVersion($file);
    // If supported file load and return the object else trigger error.
    // @todo extends the check of DBF files.
    if (in_array($version, array_keys(self::getTypes()))) {
      $db_types = self::getTypes();
      $handler = 'XBaseDbf' . $db_types[$version]['handler'];
      $object = $handler::open($file);
      return $object;
    }
    else {
      trigger_error($file . " unknown database type.", E_USER_ERROR);
    }
  }

  /**
   * Open a DBF file in write mode.
   *
   * @param string $file
   *   The file path or stream.
   *
   * @return object
   *   The XBaseDbf object from version handler.
   *
   * @todo Open a file in append mode.
   */
  static function openWrite($file) {
    $version = self::getVersion($file);
    // If supported file load and return the object else trigger error.
    // @todo extends the check of DBF files.
    if (in_array($version, array_keys(self::getTypes()))) {
      $db_types = self::getTypes();
      $handler = 'XBaseDbf' . $db_types[$version]['handler'];
      $object = $handler::openWrite($file);
      return $object;
    }
    else {
      trigger_error($file . " unknown database type.", E_USER_ERROR);
    }
  }

  /**
   * Get all supported types.
   *
   * @return array
   *   An associative array with key the hex version of DBF file and values an
   *   associative array containing:
   *   - name: The human readeble name of the Handler.
   *   - handle: The termination of the handler name.
   */
  static function getTypes() {
    return array(
      /*
      // @todo Support this type in future.
      '02' => array( // @todo Support this type in future.
        'name' => 'FoxBase',
        'handler' => 'FoxBase',
      ),
      */
      '30' => array(
        'name' => 'Visual FoxPro',
        'handler' => 'VisualFoxPro',
      ),
      /*
      // @todo Support this type in future.
      '31' => array( 
        'name' => 'Visual FoxPro With AutoIncremented Field',
        'handler' => 'VisualFoxPro',
      ),
      */
      '83' => array(
        'name' => 'dBase III',
        'handler' => 'DBaseIII',
      ),
    );
  }

  /**
   * Reads version of a DBF file.
   *
   * @param string $file
   *   The file path or stream.
   *
   * @return string
   *   The hex version of the file.
   */
  static function getVersion($file) {
    $stream = strpos($file, "://") !== FALSE;

    // Check if file provided exists.
    if (!$stream && !file_exists($file)) {
      trigger_error($file . " cannot be found.", E_USER_ERROR);
    }

    // Open file in binary mode, read version and define corect object.
    // @see XBaseDbfCore::readChar()
    if ($fp = fopen($file, "rb")) {
      $buf = unpack("C", fread($fp, 1));
      $version = dechex($buf[1]);
      fclose($fp);
    }
    else {
      // Something went wrong.
      trigger_error($file . " cannot be opened.", E_USER_ERROR);
    }
    return $version;
  }
}
