<?php
/**
 * @file
 * Plugin include file dbf for export style plugin.
 */

/**
 * DBF export style plugin.
 *
 * @ingroup views_style_plugins
 */
class views_data_export_dbf_plugin_style_export_dbf extends views_data_export_plugin_style_export {
  /**
   * Set options fields and default values.
   *
   * @return array
   *   An array of options information.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['columns'] = array('default' => array());
    $options['default'] = array('default' => '');
    // Always provide DBF's as file.
    $options['provide_file'] = array(
      'default' => TRUE,
      'translatable' => FALSE,
      'languageCode' => '',
    );
    $options['db_options'] = array(
      'version' => '131',
      'backlist' => '',
    );
    $options['default_type'] = array(
      'default' => array(
        'type' => 'C',
        'length' => '60',
      ),
    );

    return $options;
  }

  /**
   * Render the style option form.
   *
   * @see view_plugin_style_table::option_form()
   * @see http://www.dbase.com/Knowledgebase/INT/db7_file_fmt.htm
   *
   * @todo Auto get DBF types based on view fields
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $error_msg = t('You need at least one field before you can configure your table settings');
      $form['error_markup'] = array(
        '#markup' => '<div class="error messages">' . $error_msg . '</div>',
      );
      return;
    }

    // Always provide DBF files as file so user cannot configure this.
    unset($form['provide_file']);
    // Register our theme handlers for options form.
    $form['#theme'] = 'views_data_export_dbf_ui_style_plugin_table';

    // Database options
    $form['db_options'] = array(
      '#weight' => -10,
    );
    $form['db_options']['version'] = array(
      '#type' => 'select',
      '#title' => t('Database type'),
      '#description' => t('The database format of exported file.'),
      '#default_value' => isset($this->options['db_options']['version']) ? $this->options['db_options']['version'] : '131',
      '#options' => $this->db_types(),
    );

    // @todo Which is the maximum value of backlink? (263 bytes for backlist)
    // @see http://www.clicketyclick.dk/databases/xbase/format/dbf.html#DBF_NOTE_15_TARGET
    $form['db_options']['backlist'] = array(
      '#type' => 'textfield',
      '#title' => t('Backlink'),
      '#description' => t('Relative path of an associated database (.dbc) file, if any.'),
      '#default_value' => isset($this->options['db_options']['backlist']) ? $this->options['db_options']['backlist'] : '',
      '#weight' => 1,
      '#dependency' => array('edit-style-options-db-options-version' => array_keys($this->db_types('VisualFoxPro'))),
    );

    $form['db_options']['languageCode'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#description' => t('The language of dbf.'),
      '#default_value' => isset($this->options['db_options']['languageCode']) ? $this->options['db_options']['languageCode'] : '',
      '#options' => array('' => '') + $this->language_codes(),
      '#weight' => 2,
      '#dependency' => array('edit-style-options-db-options-version' => array_keys($this->db_types('VisualFoxPro'))),
    );
    $columns = $this->sanitize_columns($this->options['columns']);

    // Create an array of allowed columns from the data we know:
    $field_names = $this->display->handler->get_field_labels();

    if (isset($this->options['default'])) {
      $default = $this->options['default'];
      if (!isset($columns[$default])) {
        $default = -1;
      }
    }
    else {
      $default = -1;
    }

    // We don't translate types
    //
    // @todo - Types should be available based on db_type.
    //       - Integers also exists but we didn't implement them yet.
    //
    // @see http://devzone.advantagedatabase.com/dz/webhelp/advantage9.0/server1/dbf_field_types_and_specifications.htm
    $types = array(
      'C' => 'Character',
      'M' => 'Memo',
      'N' => 'Numeric', 
      'F' => 'Float',
      'D' => 'Date',
      'T' => 'DateTime',
      'L' => 'Logical',
      'I' => 'Index',
    );
    foreach ($columns as $field => $column) {
      // The safe id to react on length and precision.
      $safe = str_replace(array('][', '_', ' '), '-', $field);
      // the $id of the column for dependency checking.
      $id = 'edit-style-options-info-' . $safe;

      // markup for the field name
      $form['info'][$field]['name'] = array(
        '#markup' => $field_names[$field],
      );

      $form['info'][$field]['type'] = array(
        '#type' => 'select',
        '#default_value' => !empty($this->options['info'][$field]['type']) ? $this->options['info'][$field]['type'] : $this->options['default_type']['type'],
        '#options' => $types,
      );

      // @todo We set '#title' for elements to render errors on validation problems but it doesn't work.
      //
      // @see http://drupal.stackexchange.com/questions/27239/form-set-error-not-displaying-error-after-invalid-form-submission

      // Setting defaults to 0 means nothing to field description in dbf so is
      // no need to check if values where inserted in options_form_validate().
      $form['info'][$field]['length'] = array(
        '#type' => 'textfield',
        '#title' => t('Length'),
        '#title_display' => 'invisible',
        '#size' => 5,
        '#maxlength' => 5,
        '#default_value' => isset($this->options['info'][$field]['length']) ? $this->options['info'][$field]['length'] : $this->options['default_type']['length'],
        '#dependency' => array($id . '-type' => array('C', 'N', 'F')),
      );

      // @todo Select for Memo 10 or 4

      $form['info'][$field]['precision'] = array(
        '#type' => 'textfield',
        '#title' => t('Length'),
        '#title_display' => 'invisible',
        '#size' => 2,
        '#maxlength' => 2,
        '#default_value' => isset($this->options['info'][$field]['precision']) ? $this->options['info'][$field]['precision'] : 0,
        '#dependency' => array($id . '-type' => array('N', 'F')),
      );

      // @todo Feature: Merge with existing DBF.
      /*
      $form['additional'][$field]['overwrite'] = array(
        '#type' => 'checkbox',
        '#default_value' => !empty($this->options['additional'][$field]['overwrite']),
      );
      */
    }
    /*
    $form['merge_table'] = array(
      '#type' => 'textfield',
      '#title' => t('Merge with table'),
      '#description' => t('If you want to update/append views result to another table enter the table path here.'),
      '#size' => 100,
      '#default_value' => isset($this->options['merge_table']) ? $this->options['merge_table'] : '',
    );
    */

    $form['description_markup'] = array(
      '#markup' => '<div class="description form-item">' . t('Set dbf\'s columns label and types.') . '</div>',
    );
  }

  // @todo No red fields yet. Maybe I didn't understand this right.
  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    // Cleanup options if there is imput but hidden after.
    // Cleanup database backlist if not supported by database type.
    $db_options = &$form_state['values']['style_options']['db_options'];
    if (!in_array($db_options['version'], array('48', '49', '50', '245', '251'))) {
      unset($db_options['backlist']);
    }

    // Cleanup hidden(disabled) column options.
    $values = &$form_state['values']['style_options']['info'];
    $error = FALSE;
    $names = array();
    foreach ($values as $field => &$value) {
      // Clenup lengths and precisions.
      if (!in_array($value['type'], array('C', 'N', 'F'))) {
        unset($value['length']);
        unset($value['precision']);
      }
      elseif (!in_array($value['type'], array('N', 'F'))) {
        unset($value['precision']);
      }

      // Error if not integers
      if (isset($value['length']) && !is_numeric($value['length'])) {
        $names[] = $field . '][length';
        $error = TRUE;
      }
      if (isset($value['precision']) && !is_numeric($value['precision'])) {
        $names[] = $field . '][precision';
        $error = TRUE;
      }
    }

    if ($error) {
      foreach ($names as $name) {
        form_set_error($name);
      }
      form_set_error('info', t('Please insert only integer numeric values.'));
    }
  }

  /**
   * Normalize a list of columns based upon the fields that are
   * available. This compares the fields stored in the style handler
   * to the list of fields actually in the view, removing fields that
   * have been removed and adding new fields in their own column.
   *
   * - Each field must be in a column.
   * - Each column must be based upon a field, and that field
   *   is somewhere in the column.
   * - Any fields not currently represented must be added.
   * - Columns must be re-ordered to match the fields.
   *
   * @param $columns
   *   An array of all fields; the key is the id of the field and the
   *   value is the id of the column the field should be in.
   * @param $fields
   *   The fields to use for the columns. If not provided, they will
   *   be requested from the current display. The running render should
   *   send the fields through, as they may be different than what the
   *   display has listed due to access control or other changes.
   *
   * @return array
   *    An array of all the sanitized columns.
   */
  function sanitize_columns($columns, $fields = NULL) {
    $sanitized = array();
    if ($fields === NULL) {
      $fields = $this->display->handler->get_option('fields');
    }
    // Preconfigure the sanitized array so that the order is retained.
    foreach ($fields as $field => $info) {
      // Set to itself so that if it isn't touched, it gets column
      // status automatically.
      $sanitized[$field] = $field;
    }

    foreach ($columns as $field => $column) {
      // first, make sure the field still exists.
      if (!isset($sanitized[$field])) {
        continue;
      }

      // If the field is the column, mark it so, or the column
      // it's set to is a column, that's ok
      if ($field == $column || $columns[$column] == $column && !empty($sanitized[$column])) {
        $sanitized[$field] = $column;
      }
      // Since we set the field to itself initially, ignoring
      // the condition is ok; the field will get its column
      // status back.
    }

    return $sanitized;
  }

  /**
   * Tells display that we have http_headers to set.
   *
   * If this method is not set in our style plugin the display doesn't know that
   * we need headers and the parent style method is not called.
   *
   * @todo This shoul work after the render order bug in views_data_export_plugin_display_export::render()
   *       is fixed. views_data_export_dbf_plugin_style_export_dbf::render() at line 385.
   */
  function add_http_headers() {
    // We don't need any custom headers so the parent's headers are ok.
    parent::add_http_headers();
  }

  /**
   * Transfer file.
   *
   * @todo Change to add_http_headers()
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }
    // If we are not in live preview, send headers and transfer the file.
    if (empty($this->view->live_preview)) {
      $fields = $this->display->handler->get_option('fields');
      // Ensure that all fields are rendered first
      //
      // @see: views_plugin_style::get_field()
      if (!isset($this->rendered_fields)) {
        $this->render_fields($this->view->result);
      }

      $results = $this->rendered_fields;

      // Display only note excluded from display fields
      // @todo Maybe columns and rows should be trimmed for ' (quotes) using trim().
      $columns = array();
      foreach ($fields as $id => $field) {
        if (!$field['exclude']) {
          // Cleanup html chars first
          $column = array(
            'label' => htmlspecialchars_decode($field['label'], ENT_QUOTES),
          );
          // Append defaults if not set by the user
          $column += !empty($this->options['info'][$id]) ? $this->options['info'][$id] : $this->options['default_type'];
          $columns[] = $column;
        }
      }

      $rows = array();
      foreach ($results as $result) {
        $row = array();
        foreach ($result as $field_id => $cell) {
          if (!$fields[$field_id]['exclude']) {
            // Cleanup html chars first
            $row[] = htmlspecialchars_decode($cell, ENT_QUOTES);
          }
        }
        $rows[] = $row;
      }

      $tmp_dbf = drupal_tempnam('temporary://', 'vde_dbf_');
      $table = XBaseDbf::create($tmp_dbf, $columns, $this->options['db_options']);

      $table->appendRecords($rows);
      $table->close();

      // Because we call the file_transfer thats invoke drupal_exist at the end
      // of this method, the headers are not set because they are invoked after
      // the render and not before.
      //
      // @todo This is a bug in views_data_export_plugin_display_export::render().
      //       The style plugin should call parent::render() after add_http_headers().
      //       Line 381 should be moved after if $this->view->live_preview condtion.
      parent::add_http_headers();
      // File transfer calls drupal_exit() so we'll not return anything after.
      file_transfer($tmp_dbf, array());
    }
    else {
      return t('DBF Files cannot be live previewed.');
    }
  }

  function render_body() {
    $tmp_dbf = $this->display->handler->outputfile_path();

    $fields = $this->display->handler->get_option('fields');

    // Display only note excluded from display fields
    $columns = array();
    foreach ($fields as $id => $field) {
      if (!$field['exclude']) {
        $column = array(
            'label' => htmlspecialchars_decode($field['label'], ENT_QUOTES),
        );
        $columns[] = $column + $this->options['info'][$id];
      }
    }

    // Ensure that all fields are rendered first
    //
    // @see views_plugin_style::get_field()
    if (!isset($this->rendered_fields)) {
      $this->render_fields($this->view->result);
    }

    $results = $this->rendered_fields;
    $rows = array();
    foreach ($results as $result) {
      $row = array();
      foreach ($result as $field_id => $cell) {
        if (!$fields[$field_id]['exclude']) {
          $row[] = htmlspecialchars_decode($cell, ENT_QUOTES);
        }
      }
      $rows[] = $row;
    }

    // Check if we already created the file and if yes open that file, else create the database.
    if (filesize($tmp_dbf) > 0) {
      $table = XBaseDbf::openWrite($tmp_dbf);
    }
    else {
      $table = XBaseDbf::create($tmp_dbf, $columns, $this->options['db_options']);
    }
    $table->appendRecords($rows);
    $table->close();
  }

  /**
   * Overwrites default render_header to make sure nothing is returned here.
   */
  function render_header() {
    return;
  }

  /**
   * Overwrites default render_footer to make sure nothing is write here.
   */
  function render_footer() {
    return;
  }

  /**
   * Returns DBF supported language codes.
   *
   * @see http://msdn.microsoft.com/en-us/library/aa975345(v=vs.71).aspx
   */
  function language_codes() {
    $language_codes = XBaseDbfVisualFoxPro::getLanguageCodes();
    // Pass them to translate (t()).
    foreach ($language_codes as &$language_title) {
      $language_title = t($language_title);
    }
    // Sort by title first.
    natcasesort($language_codes);
    return $language_codes;
  }

  /**
   * Returns DBF databases types.
   *
   * @see http://www.dbf2002.com/dbf-file-format.html
   */
  function db_types($handler = NULL) {
    $db_types = array();
    foreach (XBaseDbf::getTypes() as $version => $props) {
      if (!is_null($handler)) {
        if ($props['handler'] == $handler) {
          $db_types[$version] = t($props['name']);
        }
      }
      else {
        $db_types[$version] = t($props['name']);
      }
    }
    natcasesort($db_types);
    return $db_types;
  }
}
