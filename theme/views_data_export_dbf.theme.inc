<?php
/**
 * @file
 * File with theme functions of views_data_export_dbf module.
 */

/**
 * Theme the form for the dbf style plugin.
 */
function theme_views_data_export_dbf_ui_style_plugin_table($variables) {
  $form = $variables['form'];

  $output = drupal_render($form['description_markup']);

  $header = array(
    t('Field'),
    t('Type'),
    t('Length'),
    t('Precision'),
  );

  $rows = array();
  foreach (element_children($form['info']) as $id) {
    $row = array();

    $row[] = drupal_render($form['info'][$id]['name']);
    $row[] = drupal_render($form['info'][$id]['type']);
    $row[] = drupal_render($form['info'][$id]['length']);
    $row[] = drupal_render($form['info'][$id]['precision']);

    $rows[] = $row;
  }

  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}
