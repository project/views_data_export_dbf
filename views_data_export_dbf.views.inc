<?php
/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implements hook_views_plugins().
 *
 * Plugin handles dbf files directly and is no need of setting theme functions.
 *
 * @see views_data_export_views_plugins()
 */
function views_data_export_dbf_views_plugins() {
  $path = drupal_get_path('module', 'views_data_export_dbf');

  $style_defaults = array(
    'path' => $path . '/plugins',
    'uses row plugin' => FALSE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'type' => 'data_export',
  );

  return array(
    'style' => array(
      'views_data_export_dbf' => array(
        'title' => t('DBF file'),
        'help' => t('Display the view as a dbf file.'),
        'handler' => 'views_data_export_dbf_plugin_style_export_dbf',
        'export headers' => array('Content-Type' => 'application/octet-stream'),
        'export feed type' => 'dbf',
        'export feed text' => 'DBF',
        'export feed file' => '%view.dbf',
        'export feed icon' => drupal_get_path('module', 'views_data_export_dbf') . '/images/dbf.png',
      ) + $style_defaults,
    ),
  );
}
